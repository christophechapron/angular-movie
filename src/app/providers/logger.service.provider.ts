

import { LOCALE_ID } from '@angular/core';
import { APP_CONFIG } from '@ngxmovie/core';
import { Jsonp } from '@angular/http';
import { Logger, EvenBetterLogger, AnotherConfigService } from '@ngxmovie/services';


const LoggerServiceFactory = (AnotherConfigService: AnotherConfigService) => {

  return new EvenBetterLogger(AnotherConfigService);
};
export let LoggerServiceProvider = {
   provide: EvenBetterLogger,
    useFactory: LoggerServiceFactory,
    deps: [AnotherConfigService]
  };
