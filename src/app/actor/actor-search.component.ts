import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActorService } from './actor.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'ngxmovie-actor-search',
  template: `
<div>
    {{person | json}}
</div>
  <router-outlet></router-outlet>`
})
export class ActorSearchComponent implements OnInit, OnDestroy  {
  sub: Subscription;
  person: any;


  constructor(
    private router: ActivatedRoute, private _ActorService: ActorService) { }

  ngOnInit() {
     this.sub =  this.router.paramMap.subscribe((params) => {
      const id = params.get('id');
      this._ActorService.getPersonDetail(id).subscribe(person => {
        this.person = person;
        console.log(person);
      });
  });

}

 ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
