import { SharedModule } from './../shared/shared.module';
import { MoviesRoutingModule } from './movies-routing.module';
import { ApiService } from '@ngxmovie/core';
import { UpcomingService } from './upcoming/upcoming.service';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { GenresComponent } from './genres/genres.component';
import { MovieComponent } from './movie/movie.component';
import { MoviesComponent } from './movies.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



console.log('`Detail` bundle loaded asynchronously');


@NgModule({
  imports: [
    CommonModule,
    MoviesRoutingModule,
    SharedModule
  ],
  declarations: [
    MoviesComponent,
    MovieComponent,
    GenresComponent,
    UpcomingComponent
  ],
    providers: [UpcomingService, ApiService]
})
export class MoviesModule { }
