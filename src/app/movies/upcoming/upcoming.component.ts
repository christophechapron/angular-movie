import { Component, OnInit } from '@angular/core';

import { UpcomingService } from './upcoming.service';

@Component({
  selector: 'ngxmovie-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css'],

})
export class UpcomingComponent implements OnInit {
  movies: Array<Object>;
  searchRes: Array<Object>;
  searchStr: string;

  constructor(private _UpcomingService: UpcomingService) {
    console.log(UpcomingService);
    this._UpcomingService.getUpComingMovies().subscribe(res => {
      this.movies = res.results;
    });
  }

  ngOnInit() {
  }


}
