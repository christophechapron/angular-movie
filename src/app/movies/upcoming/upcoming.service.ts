import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { APP_CONFIG , ApiService} from '@ngxmovie/core';
import { Jsonp , URLSearchParams} from '@angular/http';
import { Logger } from '@ngxmovie/services';
@Injectable()
export class UpcomingService  {

  constructor(public _ApiService: ApiService) {
   }

  getUpComingMovies() {
    const search = new URLSearchParams();
      console.log( this._ApiService.baseUrl);
    const url = this._ApiService.baseUrl + 'movie/upcoming';
    console.log(url);
    return this._ApiService.SearchGet(search, url);
  }
}
